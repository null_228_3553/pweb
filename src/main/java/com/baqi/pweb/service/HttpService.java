package com.baqi.pweb.service;

import com.baqi.pweb.common.BaqiException;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class HttpService {
    private static final Logger log = LoggerFactory.getLogger(HttpService.class);

    private HttpClient httpClient;
    private BasicCookieStore cookieStore;

    @PostConstruct
    public void init() {
        // post请求
        cookieStore = new BasicCookieStore();
        //设置代理
//        HttpHost proxy = new HttpHost("127.0.0.1",8888);
//        RequestConfig requestConfig = RequestConfig.custom().setProxy(proxy).build();
        httpClient = HttpClients.custom()
//                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(cookieStore)
                .setMaxConnPerRoute(50).setMaxConnTotal(100).build();
    }

    public BasicCookieStore getCookieStore() {
        return cookieStore;
    }

    public String postForString(String url) throws BaqiException {
        return this.postForString(url, null);
    }

    public String postForString(String url, Map<String, String> map) throws BaqiException {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
        try {
            if (map != null && map.size() > 0) {
                List<NameValuePair> pairs = new ArrayList();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    pairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                httpPost.setEntity(new UrlEncodedFormEntity(pairs, "UTF-8"));
            }
            HttpResponse response = this.httpClient.execute(httpPost);
            String out = EntityUtils.toString(response.getEntity(), "UTF-8");
//            log.info("接口返回：{}", out);
            return out;
        } catch (IOException e) {
            e.printStackTrace();
            throw new BaqiException("remote.api.error");
        } finally {
            httpPost.releaseConnection();
        }
    }

}
