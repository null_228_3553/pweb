package com.baqi.pweb.service;

import com.baqi.pweb.bean.Announce;
import com.baqi.pweb.common.BaqiException;
import jdk.nashorn.internal.runtime.regexp.RegExpFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class AnnounceService {

    @Autowired
    private HttpService httpService;

    //武汉市
    public List<Announce> whAnnounceList() throws BaqiException {
        String city = "武汉市";
        String qybm = "4201??";
        String district = "全市";
        return announceList(city, qybm, district);
    }

    //湖北省
    public List<Announce> hbAnnounceList() throws BaqiException {
        String city = "湖北省";
        String qybm = "42????";
        String district = "全省";
        return announceList(city, qybm, district);
    }

    public List<Announce> announceList(String city, String qybm, String district) throws BaqiException {
        // 获取cookie
        String initUrl = "http://www.ccgp-hubei.gov.cn:9040/quSer/initSearch";
        httpService.postForString(initUrl);
        // 查询结果
        String url = "http://www.ccgp-hubei.gov.cn:9040/quSer/search";
        Map<String, String> map = new HashMap();
        map.put("queryInfo.type", "xmgg");
        map.put("queryInfo.key", "幼儿园"); //搜索关键词
        map.put("queryInfo.jhhh", "");
        map.put("queryInfo.fbr", "");
        map.put("queryInfo.gglx", ""); //公告类型
        map.put("queryInfo.cglx", ""); //采购类型
//        map.put("queryInfo.cglx", "货物类"); //采购类型
        map.put("queryInfo.cgfs", ""); //采购方式
        map.put("queryInfo.city", city);
        map.put("queryInfo.qybm", qybm); //区域编码
        map.put("queryInfo.district", district);
        map.put("queryInfo.cgr", "");
        map.put("queryInfo.begin", ""); //开始时间
        map.put("queryInfo.end", "");   //结束时间
        String html = httpService.postForString(url, map);

        List<Announce> announceList = new ArrayList<Announce>();
        Document doc = Jsoup.parse(html);
        Elements lis = doc.getElementsByClass("serach-page-results-item");
        for (Element li : lis) {
            Announce announce = new Announce();
            Element a = li.getElementsByTag("a").get(0);
            // 跳转链接
            String href = a.attr("href");
            announce.setHref(href);
            // 标题
            String text = a.text();
            announce.setName(text);
            // 发布时间
            Element timeElement = li.getElementsByClass("time").get(0);
            String time = timeElement.text();
            announce.setTime(time);
            // 其他信息
            Elements infoElementList = li.getElementsByClass("type-col");
            String[] infoArr = new String[infoElementList.size()];
            for (int i = 0; i<infoElementList.size(); i++) {
                infoArr[i] = infoElementList.get(i).text();
            }
            String info = String.join("；", infoArr);
            announce.setInfo(info);
            // 类型  info.match(/公告类型：(.*?)；/)
            Pattern r = Pattern.compile("公告类型：(.*?)；");
            Matcher m = r.matcher(info);
            if (m.find()) {
                String type = m.group(1);
                announce.setType(type);
            }

            announceList.add(announce);
        }
        return announceList;
    }

}
