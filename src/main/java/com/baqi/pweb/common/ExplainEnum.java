package com.baqi.pweb.common;

public class ExplainEnum {
    public static class Status {
        public static final int Normal = 1;
        public static final int InActive = 2;
        public static final int Deleted = 3;
    }
    public static class Platform {
        public static final String All = "all";
        public static final String Ecop = "ecop";
        public static final String Fxg = "fxg";
        public static final String Buyin = "buyin";
    }
    public static class Priority {
        public static final int HighPriority = 1;
        public static final int Manager = 2;
    }
}
