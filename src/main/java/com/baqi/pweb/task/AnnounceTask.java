package com.baqi.pweb.task;

import com.baqi.lion.common.MyBatch;
import com.baqi.pweb.bean.Announce;
import com.baqi.pweb.common.BaqiException;
import com.baqi.pweb.dao.AnnounceMapper;
import com.baqi.pweb.service.AnnounceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Component
public class AnnounceTask {
    private static final Logger log = LoggerFactory.getLogger(AnnounceTask.class);

    private static String host = "smtp.163.com";
    private static String user = "milugloomy";
    private static String password = "nitai2le";
    private static String mailFrom = "milugloomy@163.com";
    private static String mailTo = "1017612520@qq.com"; // 徐倩倩
    private static String mailTo2 = "1605182385@qq.com"; // 麻爱珠

    private Properties prop;
    @Resource
    private AnnounceMapper announceMapper;
    @Autowired
    private AnnounceService announceService;


    @PostConstruct
    public void init() {
        prop = new Properties();
        prop.setProperty("mail.host", host);
        prop.setProperty("mail.transport.protocol", "smtp");
        prop.setProperty("mail.smtp.auth", "true");
    }

    @MyBatch(cron = "0 0/10 * * * ?")
    public void mailTask() throws BaqiException {
        log.info("发送请求");
        List<Announce> announceList = announceService.hbAnnounceList();
        List<Announce> mailList = new ArrayList<>();
        for (Announce announce : announceList) {
            String href = announce.getHref();
            Announce existAnnounce = announceMapper.selectByHref(href);
            if (existAnnounce == null) {
                // 插入数据库
                log.info("插入数据库");
                announceMapper.insertSelective(announce);
                mailList.add(announce);
            }
        }
        //发送邮件
        if (mailList.size() > 0) {
            log.info("发送邮件");
            sendMail(mailList);
        } else {
            log.info("无更新，不发邮件");
        }
    }

    private void sendMail(List<Announce> newList) {
        //使用JavaMail发送邮件的5个步骤
        //1、创建session
        Session session = Session.getInstance(prop);
        //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
        session.setDebug(false);
        //2、通过session得到transport对象
        Transport ts = null;
        try {
            ts = session.getTransport();
            //3、使用邮箱的用户名和密码连上邮件服务器，发送邮件时，发件人需要提交邮箱的用户名和密码给smtp服务器，用户名和密码都通过验证之后才能够正常发送邮件给收件人。
            ts.connect(host, user, password);
            //4、创建邮件
            Message message = createSimpleMail(session, newList);
            //5、发送邮件
            ts.sendMessage(message, message.getAllRecipients());
            System.out.println("邮件发送成功");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ts != null)
                    ts.close();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    private MimeMessage createSimpleMail(Session session, List<Announce> newList)
            throws Exception {
        //创建邮件对象
        MimeMessage message = new MimeMessage(session);
        //指明邮件的发件人
        message.setFrom(new InternetAddress(mailFrom));
        //指明邮件的收件人，现在发件人和收件人是一样的，那就是自己给自己发
//        String[] mailTo=ParamBusiness.getProperty("mailTo").split(",");
        message.setRecipients(Message.RecipientType.TO, new Address[]{
                new InternetAddress(mailTo), new InternetAddress(mailTo2)
        });

        String body = "";
        String title = "";
        if (newList.size() == 1) {
            String href = newList.get(0).getHref();
            title = "(新公告)" + newList.get(0).getName();
            body += "名称：" + newList.get(0).getName() + "<br/>";
            body += "时间：" + newList.get(0).getTime() + "<br/>";
            body += "类型：" + newList.get(0).getType() + "<br/>";
            body += "信息：" + newList.get(0).getInfo() + "<br/>";
            body += "链接：<a href=\"" + href + "\">" + href + "</a><br/>";
        } else {
            title = "一共有" + newList.size() + "条新公告";
            body = title + "<br/>";
            int i = 1;
            for (Announce announce : newList) {
                String href = announce.getHref();
                body += "【" + i + "】:<br/>";
                body += "名称：" + announce.getName() + "<br/>";
                body += "时间：" + announce.getTime() + "<br/>";
                body += "类型：" + announce.getType() + "<br/>";
                body += "信息：" + announce.getInfo() + "<br/>";
                body += "链接：<a href=\"" + href + "\">" + href + "</a><br/>";
                body += "<br/>";
                i++;
            }
        }
        //邮件标题
        message.setSubject(title);
        //邮件的文本内容
        message.setContent(body, "text/html;charset=UTF-8");
        //返回创建好的邮件对象
        return message;
    }
}
