package com.baqi.pweb.bean;

public class ExplainAlias {
    private Integer explainId;

    private String aliasName;

    public Integer getExplainId() {
        return explainId;
    }

    public void setExplainId(Integer explainId) {
        this.explainId = explainId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }
}