package com.baqi.pweb.bean;

public class ExplainActive {
    private Integer explainId;

    private String platform;

    private String activeApp;

    private String activeUrl;

    private String disableUrl;

    public Integer getExplainId() {
        return explainId;
    }

    public void setExplainId(Integer explainId) {
        this.explainId = explainId;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getActiveApp() {
        return activeApp;
    }

    public void setActiveApp(String activeApp) {
        this.activeApp = activeApp;
    }

    public String getActiveUrl() {
        return activeUrl;
    }

    public void setActiveUrl(String activeUrl) {
        this.activeUrl = activeUrl;
    }

    public String getDisableUrl() {
        return disableUrl;
    }

    public void setDisableUrl(String disableUrl) {
        this.disableUrl = disableUrl;
    }
}