package com.baqi.pweb.bean;

import java.util.Date;
import java.util.List;

public class Explain {
    private Integer id;

    private String name;

    private Integer status;

    private String creator;

    private Date createDate;

    private String updater;

    private Date updateDate;

    private String explainText;

    private List<ExplainActive> explainActiveList;

    private List<ExplainAlias> explainAliasList;

    public List<ExplainActive> getExplainActiveList() {
        return explainActiveList;
    }

    public void setExplainActiveList(List<ExplainActive> explainActiveList) {
        this.explainActiveList = explainActiveList;
    }

    public List<ExplainAlias> getExplainAliasList() {
        return explainAliasList;
    }

    public void setExplainAliasList(List<ExplainAlias> explainAliasList) {
        this.explainAliasList = explainAliasList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getExplainText() {
        return explainText;
    }

    public void setExplainText(String explainText) {
        this.explainText = explainText;
    }
}