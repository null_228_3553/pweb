package com.baqi.pweb.restcontroller;

import com.baqi.pweb.dao.ResourceMapper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class UserController {

    @Resource
    private ResourceMapper resourceMapper;

    @RequestMapping("/resource/get")
    @CrossOrigin(origins = "*")
    public com.baqi.pweb.bean.Resource get(int id) {
        com.baqi.pweb.bean.Resource resource = resourceMapper.selectByPrimaryKey(id);
        return resource;
    }

    @RequestMapping("/resource/list")
    @CrossOrigin(origins = "*")
    public List<com.baqi.pweb.bean.Resource> list() {
        List<com.baqi.pweb.bean.Resource> list = resourceMapper.selectList();
        return list;
    }

    @RequestMapping("/resource/add")
    @CrossOrigin(origins = "*")
    public void insert(com.baqi.pweb.bean.Resource resource) {
        resourceMapper.insertSelective(resource);
        return;
    }

    @RequestMapping("/resource/edit")
    @CrossOrigin(origins = "*")
    public void update(com.baqi.pweb.bean.Resource resource) {
        resourceMapper.updateByPrimaryKeySelective(resource);
        return;
    }

    @RequestMapping("/resource/delete")
    @CrossOrigin(origins = "*")
    public void update(int id) {
        resourceMapper.deleteByPrimaryKey(id);
        return;
    }


}
