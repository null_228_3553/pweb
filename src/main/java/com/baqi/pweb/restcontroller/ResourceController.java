package com.baqi.pweb.restcontroller;

import com.baqi.pweb.bean.User;
import com.baqi.pweb.common.BaqiException;
import com.baqi.pweb.dao.UserMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ResourceController {

	@Resource
	private UserMapper userMapper;

	@RequestMapping("/login")
	public String login(String username,String password) throws BaqiException{
		User user=userMapper.selectUserByName(username);
		if(user==null)
			throw new BaqiException("username.not.exist");
		if(!user.getPassword().equals(password))
			throw new BaqiException("password.err");
		return null;
	}


}
