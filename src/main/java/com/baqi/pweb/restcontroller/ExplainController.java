package com.baqi.pweb.restcontroller;

import com.baqi.pweb.bean.Explain;
import com.baqi.pweb.bean.ExplainActive;
import com.baqi.pweb.bean.ExplainAlias;
import com.baqi.pweb.common.ExplainEnum;
import com.baqi.pweb.dao.ExplainActiveMapper;
import com.baqi.pweb.dao.ExplainAliasMapper;
import com.baqi.pweb.dao.ExplainMapper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/explain")
public class ExplainController {

    @Resource
    private ExplainMapper explainMapper;
    @Resource
    private ExplainActiveMapper explainActiveMapper;
    @Resource
    private ExplainAliasMapper explainAliasMapper;

    @RequestMapping("/get")
    @CrossOrigin(origins = "*")
    public Explain get(@RequestBody int id) {
        Explain explain = explainMapper.selectByPrimaryKey(id);
        return explain;
    }

    @RequestMapping("/list")
    @CrossOrigin(origins = "*")
    public List<Explain> list(@RequestBody String platform) {
        List<Explain> explainList = explainMapper.selectByPlatform(platform);
        return explainList;
    }

    @RequestMapping("/add")
    @CrossOrigin(origins = "*")
    public int add(@RequestBody Explain explain) {
        explainMapper.insertSelective(explain);
        int id = explain.getId();
        List<ExplainAlias> explainAliasList = explain.getExplainAliasList();
        if (explainAliasList != null) {
            for (int i = 0; i < explainAliasList.size(); i++) {
                ExplainAlias explainAlias = explainAliasList.get(i);
                explainAlias.setExplainId(id);
                explainAlias.setAliasName(explainAlias.getAliasName());
                explainAliasMapper.insertSelective(explainAlias);
            }
        }
        List<ExplainActive> explainActiveList = explain.getExplainActiveList();
        for (int i = 0; i < explainActiveList.size(); i++) {
            ExplainActive explainActive = explainActiveList.get(i);
            explainActive.setExplainId(id);
            explainActive.setPlatform(explainActive.getPlatform());
            explainActive.setActiveApp(explainActive.getActiveApp());
            explainActive.setActiveUrl(explainActive.getActiveUrl());
            explainActive.setDisableUrl(explainActive.getDisableUrl());
            explainActiveMapper.insertSelective(explainActive);
        }
        return id;
    }

    @RequestMapping("/edit")
    @CrossOrigin(origins = "*")
    public void edit(@RequestBody Explain explain) {
        int id = explain.getId();
        explainMapper.updateByPrimaryKeySelective(explain);
        List<ExplainAlias> explainAliasList = explain.getExplainAliasList();
        if (explainAliasList != null) {
            explainAliasMapper.deleteByExplainId(id);
            for (int i = 0; i < explainAliasList.size(); i++) {
                ExplainAlias explainAlias = explainAliasList.get(i);
                explainAlias.setExplainId(id);
                explainAlias.setAliasName(explainAlias.getAliasName());
                explainAliasMapper.insertSelective(explainAlias);
            }
        }

        List<ExplainActive> explainActiveList = explain.getExplainActiveList();
        explainActiveMapper.deleteByExplainId(id);
        for (int i = 0; i < explainActiveList.size(); i++) {
            ExplainActive explainActive = explainActiveList.get(i);
            explainActive.setExplainId(id);
            explainActive.setPlatform(explainActive.getPlatform());
            explainActive.setActiveApp(explainActive.getActiveApp());
            explainActive.setActiveUrl(explainActive.getActiveUrl());
            explainActive.setDisableUrl(explainActive.getDisableUrl());
            explainActiveMapper.insertSelective(explainActive);
        }
    }

    @RequestMapping("/delete")
    @CrossOrigin(origins = "*")
    public void edit(@RequestBody int id) {
        Explain explain = new Explain();
        explain.setId(id);
        explain.setStatus(ExplainEnum.Status.Deleted);
        explainMapper.updateByPrimaryKey(explain);
    }
}
