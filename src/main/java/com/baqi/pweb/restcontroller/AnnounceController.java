package com.baqi.pweb.restcontroller;

import com.baqi.pweb.bean.Announce;
import com.baqi.pweb.common.BaqiException;
import com.baqi.pweb.service.AnnounceService;
import com.baqi.pweb.service.HttpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AnnounceController {

    @Autowired
    private AnnounceService announceService;

    @RequestMapping("/whAnnounce")
    public List<Announce> whAnnounce() throws BaqiException {
        List<Announce> list = announceService.whAnnounceList();
        return list;
    }

    @RequestMapping("/hbAnnounce")
    public List<Announce> hbAnnounce(HttpServletRequest request) throws BaqiException {
        List<Announce> list = announceService.hbAnnounceList();
        return list;
    }

}
