package com.baqi.pweb.dao;

import com.baqi.pweb.bean.ExplainActive;

public interface ExplainActiveMapper {
    int insert(ExplainActive record);

    int insertSelective(ExplainActive record);

    ExplainActive selectByPrimaryKey(Integer explainId);

    int deleteByExplainId(int id);
}