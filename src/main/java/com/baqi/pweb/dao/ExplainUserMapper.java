package com.baqi.pweb.dao;

import com.baqi.pweb.bean.ExplainUser;

public interface ExplainUserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(ExplainUser record);

    int insertSelective(ExplainUser record);

    ExplainUser selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(ExplainUser record);

    int updateByPrimaryKey(ExplainUser record);
}