package com.baqi.pweb.dao;

import com.baqi.pweb.bean.Explain;
import com.baqi.pweb.common.ExplainEnum;

import java.util.List;

public interface ExplainMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Explain record);

    int insertSelective(Explain record);

    Explain selectByPrimaryKey(Integer id);

    List<Explain> selectByPlatform(String platform);

    int updateByPrimaryKeySelective(Explain record);

    int updateByPrimaryKeyWithBLOBs(Explain record);

    int updateByPrimaryKey(Explain record);
}