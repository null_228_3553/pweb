package com.baqi.pweb.dao;

import com.baqi.pweb.bean.ExplainAlias;

public interface ExplainAliasMapper {
    int deleteByPrimaryKey(ExplainAlias key);

    int insert(ExplainAlias record);

    int insertSelective(ExplainAlias record);

    int deleteByExplainId(int id);
}